import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { createStackNavigator } from 'react-navigation';
import HomeScreen from './screens/HomeScreen'
import DetailsScreen from './screens/DetailsScreen'
import ActivityIndicatorScreen from './screens/ActivityIndicatorScreen'
import ButtonScreen from './screens/ButtonScreen'
import DrawerLayoutAndroidScreen from './screens/DrawerLayoutAndroidScreen'
import ImageScreen from './screens/ImageScreen'
import InputAccessoryViewScreen from './screens/InputAccessoryViewScreen'
import ImageBackgroundScreen from './screens/ImageBackgroundScreen'
import ListViewScreen from './screens/ListViewScreen'
import ModalScreen from './screens/ModalScreen'
import ProgressBarAndroidScreen from './screens/ProgressBarAndroidScreen'
import SectionListScreen from './screens/SectionListScreen'
import SliderScreen from './screens/SliderScreen'
import TextScreen from './screens/TextScreen'
import TextInputScreen from './screens/TextInputScreen'
import TouchableHighlightScreen from './screens/TouchableHighlightScreen'
import TouchableNativeFeedbackScreen from './screens/TouchableNativeFeedbackScreen'
import TouchableOpacityScreen from './screens/TouchableOpacityScreen'
import ViewScreen from './screens/ViewScreen';
import ViewPagerAndroidScreen from './screens/ViewPagerAndroidScreen';
import WebViewScreen from './screens/WebViewScreen'

const RootStack = createStackNavigator(
  {
    Home: {
      screen: HomeScreen,
    },
    Details: {
      screen: DetailsScreen,
    },
    ActivityIndicator: {
      screen: ActivityIndicatorScreen,
    },
    Button: {
      screen: ButtonScreen,
    },
    DrawerLayoutAndroid: {
      screen: DrawerLayoutAndroidScreen,
    },
    Image : {
      screen: ImageScreen,
    },
    InputAccessoryView: {
      screen: InputAccessoryViewScreen,
    },

    ImageBackground: {
      screen: ImageBackgroundScreen,
    },

    ListView: {
      screen: ListViewScreen,
    },

    Modal: {
      screen: ModalScreen,
    },

    ProgressBarAndroid: {
      screen: ProgressBarAndroidScreen,
    },

    SectionList: {
      screen: SectionListScreen,
    },

    Slider: {
      screen: SliderScreen,
    },

    Text: {
      screen: TextScreen,
    },
    
    TextInput: {
      screen: TextInputScreen,
    },

    TouchableHighlight: {
      screen: TouchableHighlightScreen,
    },

    TouchableNativeFeedback: {
      screen: TouchableNativeFeedbackScreen,
    },

    TouchableOpacity: {
      screen: TouchableOpacityScreen,
    },

    View: {
      screen: ViewScreen,
    },

    ViewPagerAndroid: {
      screen: ViewPagerAndroidScreen,
    },

    WebView: {
      screen: WebViewScreen,

    },

  },
  {
    initialRouteName: 'Home',
  }
);

export default class App extends React.Component {
  render() {
    return <RootStack />;
  }
}
