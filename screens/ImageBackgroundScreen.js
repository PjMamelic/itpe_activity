import React, { Component } from 'react';
import {  View, ImageBackground, Text } from 'react-native';
class DisplayAnImageBackground extends Component {
render() {
    
    
    return (
       <ImageBackground
        style={{width: 50, height: 50}}
        source={{uri: 'https://facebook.github.io/react-native/img/opengraph.png'}}
      >
          <Text>React</Text>
       </ImageBackground>
     );
    }
 }