import React, { Component } from 'react';
import { StyleSheet, Text, View, Button } from 'react-native';

class HomeScreen extends Component {
    render() {
        return (
          <View style={styles.container}>
            <Button
              title="Go to Details"
              onPress={() => this.props.navigation.navigate('Details')}
            />

            <Button
              title = "Go to Activity Indicator Screen"
              onPress={() => this.props.navigation.navigate('ActivityIndicator')}
            />

            <Button
              title = "Go to Button Screen"
              onPress={() => this.props.navigation.navigate('Button')}
            />

            <Button
              title = "Go to Drawer Layout Android Screen"
              onPress={() => this.props.navigation.navigate('DrawerLayoutAndroid')}
            />

            <Button
              title = "Go to Image Screen"
              onPress={() => this.props.navigation.navigate('Image')}
            />

             <Button
              title = "Go to Input Accessory View Screens"
              onPress={() => this.props.navigation.navigate('InputAccessoryView')}
            />

             <Button
              title = "Go to Image Background Screen"
              onPress={() => this.props.navigation.navigate('ImageBackground')}
            />
            
            <Button
              title = "Go to List View Screen"
              onPress={() => this.props.navigation.navigate('ListView')}
            />

            <Button
              title = "Go to Modal Screen"
              onPress={() => this.props.navigation.navigate('Modal')}
            />

            <Button
              title = "Go to Progress Bar Android Screen"
              onPress={() => this.props.navigation.navigate('ProgressBarAndroid')}
            />

            <Button
              title = "Go to Section List Screen"
              onPress={() => this.props.navigation.navigate('SectionList')}
            />

            <Button
              title = "Go to Slider Screen"
              onPress={() => this.props.navigation.navigate('Slider')}
            />

             <Button
              title = "Go to Text Screen"
              onPress={() => this.props.navigation.navigate('Text')}
            />

            <Button
              title = "Go to Text input Screen"
              onPress={() => this.props.navigation.navigate('TextInput')}
            />
 
            <Button
              title = "Go to Touchable Highlight Screen"
              onPress={() => this.props.navigation.navigate('TouchableHighlight')}
            /> 

            <Button
              title = "Go to Touchable Native Feedback Screen"
              onPress={() => this.props.navigation.navigate('TouchableNativeFeedback')}
            /> 

            <Button
              title = "Go to Touchable Opacity Screen"
              onPress={() => this.props.navigation.navigate('TouchableOpacity')}
            /> 

            <Button
              title = "Go to View Screen"
              onPress={() => this.props.navigation.navigate('View')}
            /> 

            <Button
              title = "Go to View Pager Android Screen"
              onPress={() => this.props.navigation.navigate('ViewPagerAndroid')}
            /> 

             <Button
              title = "Go to Web View Screen"
              onPress={() => this.props.navigation.navigate('WebView')}
            /> 
          </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#fff',
      alignItems: 'center',
      justifyContent: 'center',
    },
  });
  

HomeScreen.propTypes = {

};

export default HomeScreen;
